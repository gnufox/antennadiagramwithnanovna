from rotor import Rotor
from nanovna_mag import nanovna_mag

import time
import datetime

import os
import shutil
from os import listdir
from os.path import isfile, join

import csv
import math

import matplotlib.pyplot as plt

import threading

import numpy as np
import matplotlib.pyplot as plt

# TODO: CLEAN UP THIS MESS!!!!

# init rotor
my_rotor = Rotor("/dev/ttyUSB0")

my_rotor.goto_azimuth(-(my_rotor.MaxRange-1))
print("Rotor turning to start point, press ENTER when done")
input()

antenna_diagram = []
antenna_diagram_file = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S_antenna.diagram")

# init nanovna measurment system
my_nanovna = nanovna_mag(my_rotor)

try:
    my_nanovna.start()
    my_nanovna.wait_for_continue()
    my_nanovna.stop()
except KeyboardInterrupt:
    my_nanovna.stop()
    #exit(0)

try:
    f = open("./"+antenna_diagram_file, "w")
    for i in my_nanovna.antenna_diagram:
        f.write(f"{i[0]} {i[1]}\n")
    #endfor
    f.close()
except:
    print("error writing file")
#endtry

theta = np.array([i[0] for i in my_nanovna.antenna_diagram])
radius = np.array([i[1] for i in my_nanovna.antenna_diagram])

theta = np.deg2rad(theta)
fig, ax = plt.subplots(subplot_kw={'projection': 'polar'})
ax.plot(theta, radius)
ax.set_rmax(0.0001)
ax.set_rticks([0.5, 1, 1.5, 2])  # Less radial ticks
ax.set_rlabel_position(-22.5)  # Move radial labels away from plotted line
ax.grid(True)

ax.set_title("A line plot on a polar axis", va='bottom')
plt.show()

exit(0)